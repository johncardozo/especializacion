# Pide datos al usuario
# SIEMPRE: input retorna string
edad = input('Digite su edad... ')
# Convierte a entero
edad_entera = int(edad)
print(edad)
print(type(edad))
print(type(edad_entera))

# Suma de 2 numeros enteros
num_1 = input('Digite el numero 1: ')
num_2 = input('Digite el numero 2: ')
resultado = int(num_1) + int(num_2)
print(resultado)

# Area del triangulo
base = input('Digite el base: ')
altura = input('Digite el altura: ')

# Convierte los tipos de dato
b = float(base)
h = float(altura)

# Calcula el area del triangulo
area = (b * h) / 2

# Muestra el area en la consola
print(area)
