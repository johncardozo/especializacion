# Diccionario
persona = {
    'nombre': 'John',
    'edad': 32,
    'promedio': 4.5,
    'es_colombiano': True,
    'hobbies': ['cine', 'musica', 'taekwondo'],
    'canciones': [
        {'titulo': 'Idilio', 'artista': 'Willie Colón'},
        {'titulo': 'Bonita', 'artista': 'Diomedes Diaz'},
        {'titulo': 'Cali Pachanguero', 'artista': 'Grupo Niche'}
    ],
    'ubicacion': {
        'ciudad': 'Bogota',
        'pais': 'Colombia'
    }
}
# Accede a una clave
n = persona['nombre']
print(n)
p = persona['promedio']
print(p)
# Recorre una clave de lista
for h in persona['hobbies']:
    print(h)
# Accede a una clave de una diccionario
print(persona['ubicacion']['pais'])
# Recorre una lista de diccionarios
for artista in persona['canciones']:
    print(artista['artista'])

print(type(persona))

print(persona)