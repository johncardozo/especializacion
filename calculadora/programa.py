from ui import (
    menu, 
    pedir_valores_usuario, 
    mostrar_resultado, 
    bye_bye, 
    opcion_erronea)
from calculo import (
    sumar, 
    restar, 
    dividir, 
    multiplicar)

# Definicion de constantes
SUMAR = 1
RESTAR = 2
MULTIPLICAR = 3
DIVIDIR = 4
SALIR = 5

opcion = 0
while opcion != SALIR:
    # Pide la opcion al usuario
    opcion = menu()
    if opcion == SUMAR:
        # pide valores al usuario
        num1, num2 = pedir_valores_usuario()
        # Suma los valores
        resultado = sumar(num1, num2)
        # Muestra el resultado
        mostrar_resultado(resultado)
    elif opcion == RESTAR:
        # pide valores al usuario
        num1, num2 = pedir_valores_usuario()
        # Restar los valores
        resultado = restar(num1, num2)
        # Muestra el resultado
        mostrar_resultado(resultado)        
    elif opcion == MULTIPLICAR:
        # pide valores al usuario
        num1, num2 = pedir_valores_usuario()
        # Multiplicar los valores
        resultado = multiplicar(num1, num2)
        # Muestra el resultado
        mostrar_resultado(resultado)
    elif opcion == DIVIDIR:
        # pide valores al usuario
        num1, num2 = pedir_valores_usuario()
        # Dividir los valores
        resultado = dividir(num1, num2)
        # Muestra el resultado
        mostrar_resultado(resultado)
    elif opcion == SALIR:
        bye_bye()
    else:
        opcion_erronea()
