from objetos import Persona

# Pide datos al usuario
name = input("Digite el nombre de la persona: ")
age = int(input('Digite la edad: '))

# Crea el objeto
persona_1 = Persona(name, age)

# Obtiene el atributo del objeto
print(persona_1.nombre)

# Invoca el método del obeto
print(persona_1.es_adulto())
