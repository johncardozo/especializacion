
x = 0   # inicializacion
while x < 5:    # expresion booleana
    print(x)    # instrucciones
    x = x + 1   # instruccion de rompimiento

print('se terminó el while')
print('=' * 20)

valor = 0
# Recorre los numeros hasta 20
while valor <= 10:
    # Verifica que el valor par o impar
    if valor % 2 == 0:
        print(f'El valor {valor} es par')
    else:
        print(f'El valor {valor} es impar')
    # Incrementa el valor
    valor = valor + 1

print('-' * 20)

for numero in range(5):
    print(numero)
print('=' * 20)
for numero in range(15, 25):
    print(numero)
print('=' * 20)
for numero in range(2, 18, 2):
    print(numero)
print('-' * 20)

lista = ['oslo', 'ny', 'caracas']
# recorrido de lista con for
for valor in lista: 
    print(valor)

print('-' * 20)

# recorrido de lista con while
indice = len(lista) - 1
while indice >= 0:
    print(lista[indice])
    indice = indice - 1

print("#" * 20)

matriz = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
fila = 0
# Recorre las filas
while fila < 3:
    elem = 0
    # Recorre las columnas
    while elem < 3:
        # Imprime el valor actual
        print(matriz[fila][elem], end='\t')
        # Incrementa el elemento
        elem = elem + 1
    print('\n')
    # Incrementa la fila
    fila = fila + 1
print('!' * 20)
for fila in matriz:
    for elem in fila:
        print(elem, end='\t')
    print('\n')
