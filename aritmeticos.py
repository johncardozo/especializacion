a = 5
b = 10
# sumar
c = a + b
print(c)
# restar
d = a - b
print(d)
# multiplicar
e = a * b
print(e)
# division
f = a / b
print(f)
# modulo
g = a % b
print(g)
# division entera
h = 20 // 3
print(h)
# potencia
i = b ** a
print(i)
# expresion
j = ((a + b) * (b - 4)) / 2
print(j)
