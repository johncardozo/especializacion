from libreria import ListaReproduccion, Cancion

# Crea la lista de reproduccion
lista = ListaReproduccion('Rock')

opcion = 0

while opcion != 3:
    # Muestra le menu de opciones
    print('1. Agregar cancion')
    print('2. Ver lista de reproducción')
    print('3. Salir')
    opcion = int(input('Digite la opcion: '))
    # Opcion: agregar canción 
    if opcion == 1:
        # Pide los datos de la canción
        t = input("Digite el titulo de la cancion")
        a = input('Digite el nombre del artista: ')
        # Crea el objeto cancion
        cancion = Cancion(t, a)
        # Agrega la canción a la lista
        lista.canciones.append(cancion)
    elif opcion == 2:

        print(f'Lista: {lista.nombre}')
        print('Artista\t\t\tCanción')
        for cancion in lista.canciones:
            print(f'{cancion.titulo}\t\t\t{cancion.artista}')

        print(f'La lista tiene {lista.cantidad_canciones()} canciones')

    elif opcion == 3:
        print('Gracias por utilizar la rockola')
    else:
        print('Opción no valida')

