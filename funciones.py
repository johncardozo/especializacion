# Definicion de funcion
def mostrar_mensaje():
    print('hola')

def mostrar_datos():
    x = 1
    y = 2
    print(x, y)

def sumar(num1, num2):
    '''
    Esta funcion suma 2 numeros
    '''
    resultado = num1 + num2
    return resultado

# Llamar una funcion
mostrar_mensaje()
mostrar_datos()

r = sumar(4, 6)
print(r)
r = sumar(4, -1)
print(r)


