edad = 18

# Una opcion
if edad == 18:
    print('Es mayor de edad...')
    print('Enhorabuena!!!')

print('Termino el programa!')
print('=' * 20)
# dos opciones
age = 15
if age >= 18:
    print('It\'s an adult')
else:
    print('It\'s a teenager')
print('?' * 20)
# Multiples opciones
valor = 8
if valor >= 0 and valor <= 5:
    print('esta entre 0 y 5')
elif valor >= 10 and valor <= 20:
    print('esta entre 10 y 20')
elif valor >= 6 and valor <= 9:
    print('esta entre 6 y 9')
else:
    print('no es ninguno de los anteriores')
     