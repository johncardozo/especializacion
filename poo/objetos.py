# Definición de la clase
class Persona:
    # Atributos
    nombre = ''
    edad = 0
    # Método constructor
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def es_adulto(self):
        '''
        Retorna True si la persona es adulto y False de lo contrario
        '''
        return self.edad >= 18
