
def sumar(num1, num2):
    '''
    Esta función suma dos valores numéricos
    '''
    return num1 + num2

def restar(num1, num2):
    '''
    Esta función resta dos valores numéricos
    '''
    return num1 - num2

def multiplicar(num1, num2):
    '''
    Esta función multiplica dos valores numéricos
    '''
    return num1 * num2

def dividir(num1, num2):
    '''
    Esta función divide dos valores numéricos
    '''
    return num1 / num2