class ListaReproduccion():
    nombre = ''
    canciones = []

    def __init__(self, nombre):
        self.nombre = nombre
        self.canciones = []

    def cantidad_canciones(self):
        '''
        Retorna la cantidad de canciones en la lista de reproducción
        '''
        return len(self.canciones)


class Cancion:
    titulo: ''
    artista: ''

    def __init__(self, titulo, artista):
        self.titulo = titulo
        self.artista = artista