# # String
# cadena = 'universidad'
# print(cadena)
# # Combinacion enre " y '
# frase = "I can't read"
# print(frase)
# # \n
# parrafo = 'Universidad\nSanto\nTomas'
# print(parrafo)
# # raw string
# ruta = r'c:\documentos\nomina\datos.xls'
# print(ruta)
# # combinacion de \n y \t
# datos = 'id\tnombres\n1\tjohn\n2\tcata'
# print(datos)
# # Acceso por subindice
# primer = cadena[0]
# print(primer)
# segundo = cadena[1]
# print(segundo)
# ultimo = cadena[-1]
# print(ultimo)
# antepenultimo = cadena[-2]
# print(antepenultimo)
# subcadenas
cadena = 'santo tomas de aquino'
print(cadena)
# cadenita1 = cadena[3:8] # desde el 3 hasta el 7
# print(cadenita1)
# cadenita2 = cadena[:8] # desde el principio hasta el 7
# print(cadenita2)
# cadenita3 = cadena[3:] # desde el 3 hasta el final
# print(cadenita3)
# metodos
longitud = len(cadena)
print(longitud)
termina = cadena.endswith('no')
print(termina)
empieza = cadena.startswith('san')
print(empieza)
esta = 'de' in cadena   
print(esta)
posicion = cadena.find('de')
print(posicion)
titulo = cadena.title()
print(titulo)
cantidad = cadena.count('t')
print(cantidad)
nueva = cadena.replace('a', '4')
print(nueva)
minus = cadena.lower()
mayus = cadena.upper()
print(minus, mayus)

clave = 'abc123'
test1 = clave.isalnum()
print(test1)
test2 = clave.isalpha()
print(test2)
test3 = clave.isdecimal()
print(test3)
test4 = clave.islower()
print(test4)
test5 = clave.isupper()
print(test5)

