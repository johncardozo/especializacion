import json

# Lista de diccionarios
canciones = [
    {'titulo': 'Idilio', 'artista': 'Willie Colón'},
    {'titulo': 'Bonita', 'artista': 'Diomedes Diaz'},
    {'titulo': 'Cali Pachanguero', 'artista': 'Grupo Niche'}
]
Diccionario
persona = {
    'nombre': 'John',
    'edad': 20
}
# Elimina la llave de un diccionario
del persona['edad']
# Verifica si la llave existe dentro del diccionario
if 'edad' in persona:
    print('si tiene edad')

print(persona)

# LEER DE UN ARCHIVO JSON
# Abre el archivo en modo escritura
archivo = open('datos.json', 'w')
# Guarda el dicconario en el archivo
json.dump(persona, archivo)
# Cerrar el archivo
archivo.close()

# ESCRIBIR EN EL ARCHIVO
# Abre el archivo en modo lectura
archivo = open('datos.json', 'r')
# Carga la información del archivo en un diccionario
datos = json.load(archivo)
# Cierra el archivo
archivo.close()

# Imprime el tipo de dato
print(type(datos))
# Imprime el diccionario
print(datos)


