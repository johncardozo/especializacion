ciudades = [
    'londres', 
    'ny', 
    'roma', 
    'paris', 
    'ny'
]
print(ciudades)
# Cantidad de ocurrencias en la lista
cantidad = ciudades.count('ny')
print(cantidad)
# Indice del elemento en la lista
indice = ciudades.index('paris')
print(indice)
# Invierte la lista
ciudades.reverse()
print(ciudades)
# Agregar al final de la lista
ciudades.append('bogota')
print(ciudades)
# Ordenar la lista
ciudades.sort()
print(ciudades)
# Elimina el ultimo elemento
ciudades.pop()
print(ciudades)
# Elimina un elemento de la lista
ciudades.remove('bogota')
print(ciudades)
