# Variable de tipo entero
a = 3
print(a)
# Variable de tipo real
b = 5.6
print(b)
# Variable de tipo string
c = 'santo tomas'
print(c)
# Variable de tipo boolean
d = True
print(d)
# Consultar el tipo de las variables
tipo = type(a)
print(tipo)


