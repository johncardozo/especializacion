# matrices
lista = [ 
    [1, 2, 3], 
    [4, 5, 6], 
    [7, 8, 9]
]
# toda la matriz
print(lista)        
# la lista 2 de la matriz
print(lista[2]) 
# al elemento 1 de la lista 2    
print(lista[2][1])

# Filas de la matriz
lista1 = [1, 2, 3]
lista2 = [4, 5, 6]
lista3 = [7, 8, 9]

# Matriz basada en variables
matrix = [lista1, lista2, lista3]
print(matrix)