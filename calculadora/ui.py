def menu():
    print('\nCALCULADORA')
    print('1. Sumar')
    print('2. Restar')
    print('3. Multiplicar')
    print('4. Dividir')
    print('5. Salir')
    opcion = int(input('Digite su opcion...'))
    return opcion

def pedir_valores_usuario():
    '''
    Pide datos al usuario
    '''
    num1 = int(input('Digite el numero 1: '))
    num2 = int(input('Digite el numero 2: '))
    return num1, num2

def mostrar_resultado(valor):
    print(f'El resultado es {valor}')

def bye_bye():
    print('Muchas gracias por utilizar nuestros servicios')

def opcion_erronea():
    print('La opcion digitada no es valida')