
lista_vacia = []
lista = [10, 20, 30, 40]
print(lista)

# Acceso por índice
valor1 = lista[2]
print(valor1)
# Modifica una posición de la lista
lista[1] = 200
print(lista)
# Obtiene el ultimo elemento
ultimo = lista[-1]
print(ultimo)
# Obtiene una sección de la lista
seccion = lista[1:3] # 4 no incluida
print(seccion)
# Obtiene la longitud de la lista
longitud = len(lista)
print(longitud)
# Cantidad de ocurrencias en la lista
cantidad = lista.count(200)
print(cantidad)
# Concatenar listas
lista1 = [True, 2, 3.1416]
lista2 = [4, 'a', 2]
lista_resultado = lista1 + lista2
print(lista_resultado)
# Agrega un elemento a la lista
lista_resultado.append(1982)
print(lista_resultado)
# Inserta un elemento a la lista
lista_resultado.insert(3, 700)
print(lista_resultado)
# Modifica una sección de la lista
lista_resultado[1:5] = ['A', 'B', 'C', 'D']
print(lista_resultado)
# Elimina una sección de la lista
lista_resultado[2:5] = []
print(lista_resultado)

