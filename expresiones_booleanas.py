# AND = TODAS deben ser verdadero
# T && T = T
# T && F = F
# F && T = F
# F && F = F
# OR = AL MENOS UNA debe ser verdadero
# T || T = T
# T || F = T
# F || T = T
# F || F = F

edad = 18

# print(edad > 18)    # False
# print(edad >= 18)   # True
# print(edad < 18)    # False
# print(edad <= 18)   # True
# print(edad == 18)   # True
# print(edad != 18)   # False
# print(not edad == 18)   # False

x = 10
print(x > 5 and x < 9)
print(x > 5 and x < 15)
print(x > 5 or x < 9)
print(x < 5 or x < 9)








